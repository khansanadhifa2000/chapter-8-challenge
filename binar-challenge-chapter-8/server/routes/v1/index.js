const playerRouter = require("./player.routes");
const v1 = require("express").Router();
const swaggerUI = require('swagger-ui-express')
const openApiJson = require('./openapi.json')

v1.get("/", (_, res) => {
  res.send("from v1");
});

v1.use("/players", playerRouter);

v1.use("/doc", swaggerUI.serve, swaggerUI.setup(openApiJson))

module.exports = v1;
