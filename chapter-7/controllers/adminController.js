const {players} = require('../models')

module.exports = {
    dashboard: async (req, res) => {
        try{
            players.findAll()
                .then(players => {
                    res.render('dashboard', {players})
                })
        } catch {
            console.error(error);
            res.status(500).json({ message: 'Internal server error' });
        }
    }
}