const { players } = require("../models")
const passport = require('../lib/passport')

module.exports = {
    register: (req, res, next) => {
        // res.redirect(`profile/${players.id}`)
        players.register(req.body).then(() => {
            res.redirect('/loginForm')
        }).catch (error => {
            next(error)
            console.log(error)
            res.status(500).json({message: 'server error'});
        })
    },
    // login: (req, res) => {
        // try {
        //     const {username, password} = req.body;

        //     const player = await players.findOne({ where: { username }});

        //     if (!player) {
        //         return res.status(404).json({ message: 'player not found' });
        //     }

        //     const passwordsMatch = await bcrypt.compare(password, player.password);

        //     if (!passwordsMatch) {
        //         return res.status(401).json({ message: 'invalid credentials' })
        //     }
            
        //     res.redirect('/hello')
        // } catch (error) {
        //     res.status(500).json({ message: 'server error' });
        // }
    // },
    login: passport.authenticate('local', {
        successRedirect: '/hello',
        failureRedirect: '/login',
        failureFlash: true
    }),
    hello: (req, res) => {
        try {
            res.render('testview')
        } catch (error) {
            res.status(500).json({ message: 'server error' })
        }
    }

}
