const express = require('express');
const app = express();
const session = require('express-session');
const flash = require('express-flash');
const passport = require('./lib/passport');
const route = require('./routes/route');
const port = 3001;

app.use(express.urlencoded({ extended: false }));
app.set('view engine', 'ejs');
app.use(express.static('public'));

app.use(route);

app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false
}))

app.use(passport.initialize())
app.use(passport.session())

app.use(flash)

//internal server error
app.use((err, req, res, next) => {
  res.status(500).json({
    status: 'fail',
    errors: err.message,
  });
});

//missing page
app.use((req, res, next) => {
  res.status(401).json({
    status: 'fail',
    errors: 'Are you lost?',
  });
});

//listen
app.listen(port, () => {
  console.log(`Web started at port : ${port}`);
});
