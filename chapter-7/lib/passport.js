const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const { players } = require('../models')

async function authenticate(username, password, done) {
    try{ 
        const player = await players.authenticate({username, password})

        return done(null, player)
    } catch (error) {
        return done(null, false, {
            message: error
        })
    }
}

passport.use(
    new localStrategy({usernameField: 'username', passwordField: 'password'}, authenticate)
)

passport.serializeUser(
    (player, done) => done(null, player.id)
)

passport.deserializeUser(
    async (id, done) => done(null, await players.findByPk(id))
)

module.exports = passport