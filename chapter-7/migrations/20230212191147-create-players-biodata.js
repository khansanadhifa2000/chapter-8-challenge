'use strict';
/** @type {import('sequelize-cli').Migration} */

// const players = require('../models/players')

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('players_biodata', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      biodata: {
        type: Sequelize.TEXT
      },
      // playerId: {
      //   type: Sequelize.INTEGER,
      //   allowNull: false,
      //   references: {
      //     model: players,
      //     key: 'playerId',
      //   },
      //   onUpdate: 'CASCADE',
      //   onDelete: 'CASCADE',
      // },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    // players_biodata.belongsTo(players, {as: 'players', foreignKey: 'playerId'});
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('players_biodata');
  }
};