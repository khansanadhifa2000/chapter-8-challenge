'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt');

// const players_biodata = require ('./players_biodata')
// const players_history = require ('./players_history')

module.exports = (sequelize, DataTypes) => {
  class players extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // define association here
      // players.hasOne(players_biodata, { 
      //   foreignKey: {
      //     allowNull: false,
      //   }
      //  });

      // players.hasOne(players_history, { 
      //   foreignKey: {
      //     allowNull: false,
      //   }
      //  });

    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = async ({name, username, email, password}) => {

      //if username already exist
      const existingPlayer = await this.findOne({where: {username}})

      if(existingPlayer) return Promise.reject('username already taken, please choose a different username')

      const encryptedPassword = this.#encrypt(password);

      return this.create({name, username, email, password: encryptedPassword})
    }

    checkPassword = (password) => bcrypt.compareSync(password, this.password)

    static authenticate = async ({username, password}) => {
      try{
        const player = await this.findOne({where: {username}})

        if(!player) return Promise.reject('player not found')

        const isPasswordValid = player.checkPassword(password)
        
        if(!isPasswordValid) Promise.reject('wrong password')

        return Promise.resolve(player)
      } catch {
        return Promise.reject(err)
      }
    }

  }
  players.init({
    name: DataTypes.STRING,
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'players',
    underscored: true,
  });
  return players;
};