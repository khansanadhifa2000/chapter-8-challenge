'use strict';
const {
  Model
} = require('sequelize');

const players = require('./players');

module.exports = (sequelize, DataTypes) => {
  class players_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      
      // players_biodata.belongsTo(players, {
      //   foreignKey: {
      //     name: 'playerId',
      //     allowNull: false,
      //     onDelete: 'CASCADE'
      //   }
      // });
      // players_biodata.belongsTo(players);
      // players.hasOne(players_biodata);
    }
  }
  players_biodata.init({
    biodata: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'players_biodata',
    underscored: true,
  });

  return players_biodata;
};