'use strict';
const {
  Model
} = require('sequelize');

// const players = require('./players');

module.exports = (sequelize, DataTypes) => {
  class players_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      // players_history.belongsTo(players);

      // players_history.belongsTo(players, { 
      //   onDelete: 'CASCADE' 
      // });
      // players.hasOne(players_history);
    }
  }
  players_history.init({
    play: DataTypes.INTEGER,
    win: DataTypes.INTEGER,
    lose: DataTypes.INTEGER,
    draw: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'players_history',
    underscored: true,
  });

  return players_history;
};