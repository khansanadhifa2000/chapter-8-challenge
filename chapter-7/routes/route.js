const express = require('express')
const adminController = require('../controllers/adminController')
const gameController = require('../controllers/gameController')
const homeController = require('../controllers/homeController')
const playerController = require('../controllers/playerController')
const restrict = require('../middlewares/restrict');
const route = express.Router()

route.get('/', homeController.chapter3)
route.get('/registerForm', homeController.registerForm)
route.post('/register', playerController.register)

route.get('/loginForm', homeController.loginForm)
route.post('/login', playerController.login)
route.get('/hello', restrict, playerController.hello)

// route.get('/profile/:id', restrict, playerController.profile)
// route.get('/update/:id', restrict, playerController.updateForm)
// route.post('/update/:id', restrict, playerController.update)
// route.delete('/delete/:id', restrict, playerController.destroy)

route.get('/game', gameController.game)

route.get('/dashboard', adminController.dashboard)


module.exports = route