
// import './App.css';
import FrontPage from './FrontPage';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <FrontPage></FrontPage>
      </header>
    </div>
  );
}

export default App;
