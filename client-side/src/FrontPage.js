import React, { useRef, useState } from "react";
import { Nav, Navbar, Container, NavDropdown, Card, Form, Button, Row } from 'react-bootstrap';


function FrontPage() {
    const emailInput = useRef(null);
    const usernameInput = useRef(null);
    const passwordInput = useRef(null);

    const [email, setEmail] = useState();
    const [username, setName] = useState();
    const [password, setPassword] = useState();
    const [exp, setExp] = useState(null);
    const [lvl, setLvl] = useState(null);

    const handleSubmit = (e) => {
        e.preventDefault();

        setEmail(emailInput.current.value)
        setName(usernameInput.current.value)
        setPassword(passwordInput.current.value)
        setExp(0)
        setLvl(0)
    }

    const editEmail = useRef();
    const editName = useRef();
    const editPassword = useRef();

    const [emailEdit, setEmailEdit] = useState('userbinar@gmail.com');
    const [nameEdit, setNameEdit] = useState('userbinar');
    const [passwordEdit, setPasswordEdit] = useState('binar');

    const handleEdit = (e) => {
        e.preventDefault();

        setEmailEdit(editEmail.current.value)
        setNameEdit(editName.current.value)
        setPasswordEdit(editPassword.current.value)
    }

    const findEmail = useRef(null);

    const [emailFound, setEmailFound] = useState();
    const [usnEmail, setUsnEmail] = useState();
    const [passEmail, setPassEmail] = useState();
    const [expEmail, setExpEmail] = useState();
    const [lvlEmail, setLvlEmail] = useState();

    const handleFindEmail = (e) => {
        e.preventDefault();

        setEmailFound(findEmail.current.value)
        setUsnEmail('userbinar')
        setPassEmail('binar')
        setExpEmail(0)
        setLvlEmail(0)
    }

    const findUsername = useRef(null);

    const [emailUsn, setEmailUsn] = useState();
    const [usnFound, setUsnFound] = useState();
    const [passUsn, setPassUsn] = useState();
    const [expUsn, setExpUsn] = useState();
    const [lvlUsn, setLvlUsn] = useState();

    const handleFindUsername = (e) => {
        e.preventDefault();

        setEmailUsn('userbinar@gmail.com')
        setUsnFound(findUsername.current.value)
        setPassUsn('binar')
        setExpUsn(0)
        setLvlUsn(0)
    }

    const findExp = useRef(null);

    const [emailExp, setEmailExp] = useState();
    const [usnExp, setUsnExp] = useState();
    const [passExp, setPassExp] = useState();
    const [expFound, setExpFound] = useState();
    const [lvlExp, setLvlExp] = useState();

    const handleFindExp = (e) => {
        e.preventDefault();

        setEmailExp('userbinar@gmail.com')
        setUsnExp('userbinar')
        setPassExp('binar')
        setExpFound(findExp.current.value)
        setLvlExp(0)
    }
    
    const findLvl = useRef(null);

    const [emailLvl, setEmailLvl] = useState();
    const [usnLvl, setUsnLvl] = useState();
    const [passLvl, setPassLvl] = useState();
    const [expLvl, setExpLvl] = useState();
    const [lvlFound, setLvlFound] = useState();

    const handleFindLvl = (e) => {
        e.preventDefault();

        setEmailLvl('userbinar@gmail.com')
        setUsnLvl('userbinar')
        setPassLvl('binar')
        setExpLvl(100)
        setLvlFound(findLvl.current.value)
    }

    return (
        <>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="#home">Home</Nav.Link>
                        <Nav.Link href="#link">Link</Nav.Link>
                        <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">
                            Another action
                        </NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#action/3.4">
                            Separated link
                        </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

            <Container>
                <Row>
                    <Card className="col-6 m-3">
                        <Card.Body>
                            <Card.Title>Sign Up</Card.Title>
                            <Card.Text>
                                <Form onSubmit={handleSubmit}>
                                    <Form.Group className="mb-3" controlId="email">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control ref={emailInput} type="email" placeholder="Enter email" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="name">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control ref={usernameInput} type="text" placeholder="Enter name" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="formBasicPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control ref={passwordInput} type="password" placeholder="Password" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit">
                                        Sign Up
                                    </Button>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card className="col-5 m-3">
                        <Card.Body>
                            <Card.Title>Data Player</Card.Title>
                            Email: {email} <br/>
                            Username: {username} <br/>
                            Password: {password} <br/>
                            Experience: {exp} <br/>
                            Lvl: {lvl}
                        </Card.Body>
                    </Card>
                </Row>
                
                <Row>
                    <Card className="col-6 m-3">
                        <Card.Body>
                            <Card.Title>Edit Profile</Card.Title>
                            <Card.Text>
                                <Form onSubmit={handleEdit}>
                                    <Form.Group className="mb-3" controlId="email">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control ref={editEmail} type="email" placeholder="Enter email" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="name">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control ref={editName} type="text" placeholder="Enter name" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="formBasicPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control ref={editPassword} type="password" placeholder="Password" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit">
                                        Edit Profile
                                    </Button>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card className="col-5 m-3">
                        <Card.Body>
                            <Card.Title>Edit Data Player</Card.Title>
                            Email: {emailEdit} <br/>
                            Username: {nameEdit} <br/>
                            Password: {passwordEdit} <br/>
                            Experience: 0 <br/>
                            Lvl: 0
                        
                        </Card.Body>
                    </Card>
                </Row>

                <Row>
                    <Card className="col-6 m-3">
                        <Card.Body>
                            <Card.Title>Find Profile (email)</Card.Title>
                            <Card.Text>
                                <Form onSubmit={handleFindEmail}>
                                    <Form.Group className="mb-3" controlId="email">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control ref={findEmail} type="email" placeholder="Enter email" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit">
                                        Find
                                    </Button>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card className="col-5 m-3">
                        <Card.Body>
                            <Card.Title>Find Player</Card.Title>
                            Email: {emailFound} <br/>
                            Username: {usnEmail} <br/>
                            Password: {passEmail} <br/>
                            Experience: {expEmail} <br/>
                            Lvl: {lvlEmail} <br/>
                        </Card.Body>
                    </Card>
                </Row>

                <Row>
                    <Card className="col-6 m-3">
                        <Card.Body>
                            <Card.Title>Find Profile (username)</Card.Title>
                            <Card.Text>
                                <Form onSubmit={handleFindUsername}>
                                    <Form.Group className="mb-3" controlId="username">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control ref={findUsername} type="text" placeholder="Enter username" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit">
                                        Find
                                    </Button>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card className="col-5 m-3">
                        <Card.Body>
                            <Card.Title>Find Player</Card.Title>
                            Email: {emailUsn} <br/>
                            Username: {usnFound} <br/>
                            Password: {passUsn} <br/>
                            Experience: {expUsn} <br/>
                            Lvl: {lvlUsn} <br/>
                        </Card.Body>
                    </Card>
                </Row>
                
                <Row>
                    <Card className="col-6 m-3">
                        <Card.Body>
                            <Card.Title>Find Profile (Experience)</Card.Title>
                            <Card.Text>
                                <Form onSubmit={handleFindExp}>
                                    <Form.Group className="mb-3" controlId="Experience">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control ref={findExp} type="text" placeholder="Enter experience" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit">
                                        Find
                                    </Button>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card className="col-5 m-3">
                        <Card.Body>
                            <Card.Title>Find Player</Card.Title>
                            Email: {emailExp} <br/>
                            Username: {usnExp} <br/>
                            Password: {passExp} <br/>
                            Experience: {expFound} <br/>
                            Lvl: {lvlExp} <br/>
                        </Card.Body>
                    </Card>
                </Row>

                <Row>
                    <Card className="col-6 m-3">
                        <Card.Body>
                            <Card.Title>Find Profile (Level)</Card.Title>
                            <Card.Text>
                                <Form onSubmit={handleFindLvl}>
                                    <Form.Group className="mb-3" controlId="level">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control ref={findLvl} type="text" placeholder="Enter level" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit">
                                        Find
                                    </Button>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card className="col-5 m-3">
                        <Card.Body>
                            <Card.Title>Find Player</Card.Title>
                            Email: {emailLvl} <br/>
                            Username: {usnLvl} <br/>
                            Password: {passLvl} <br/>
                            Experience: {expLvl} <br/>
                            Lvl: {lvlFound} <br/>
                        </Card.Body>
                    </Card>
                </Row>
            </Container>
            


                
        </>
    )
}

export default FrontPage